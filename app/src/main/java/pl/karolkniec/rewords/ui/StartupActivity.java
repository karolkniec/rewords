package pl.karolkniec.rewords.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.util.DatabaseHelper;

public class StartupActivity extends AppCompatActivity {

    private static final String TAG = "StartupActivity";

    private SignInButton signInButton;
    private TextView startupUserNameTextView;
    private TextInputLayout startupUsernameLayout;
    private Button signInOfflineBtn;
    private DatabaseHelper dbHelper;

    GoogleApiClient mGoogleApiClient;
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseFirestore firebaseFirestore;

    private static final int RC_SIGN_IN = 2;

    @Override
    protected void onStart() {
        super.onStart();

        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        mAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        signInButton = (SignInButton) findViewById(R.id.startupGoogleSignIn);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() != null) {
                    finish();
                }
            }
        };

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(StartupActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        startupUserNameTextView = (TextView) findViewById(R.id.startupUsername);
        startupUsernameLayout = (TextInputLayout) findViewById(R.id.startupUsernameLayout);
        signInOfflineBtn = (Button) findViewById(R.id.startupCreateBtn);
        dbHelper = new DatabaseHelper(this);

        boolean checkUserExistse = dbHelper.checkUserExists();
        if(checkUserExistse == true) {
            startupUsernameLayout.setVisibility(View.INVISIBLE);
            signInOfflineBtn.setText(getString(R.string.startup_sign_in_offline_btn_text));
            Log.v(TAG, "Offline User exists");
        } else {
            Log.v(TAG, "Offline User does not exists");
        }
    }

    // region <-- GoogleAuth -->

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                Toast.makeText(StartupActivity.this, getString(R.string.startup_auth_went_wrong_toast), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            final FirebaseUser user = mAuth.getCurrentUser();
                            firebaseFirestore.collection("Users").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if(task.isSuccessful()) {
                                        Log.v(TAG, "Founded Users collection");
                                        if(task.getResult().exists()) {
                                            Log.v(TAG, "User information already exist on database");
                                        } else {
                                            Map<String, Object> userMap = new HashMap<>();
                                            userMap.put("DisplayName", user.getDisplayName());
                                            userMap.put("Email", user.getEmail());
                                            userMap.put("Level", 1);
                                            userMap.put("Experience", 0);

                                            firebaseFirestore.collection("Users")
                                                    .document(user.getUid()).set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()) {
                                                        Log.v(TAG, "Successfully added user information into database");
                                                    } else {
                                                        String error = task.getException().getMessage();
                                                        Log.v(TAG, error);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(StartupActivity.this, getString(R.string.startup_auth_went_wrong_toast), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    // endregion

    public void signInOffline(View view) {

        if(dbHelper.checkUserExists()) {
            Log.v(TAG, "Offline User exists");
            if(dbHelper.signInOffline()) {
                Log.v(TAG, "Signed in Success");
                finish();
            }
        } else {
            String username = startupUserNameTextView.getText().toString();

            if(!TextUtils.isEmpty(username)) {

                boolean result = dbHelper.addUser(username);
                if(result == true) {
                    Log.v(TAG, "Offline User Added Successfully");
                } else {
                    Log.v(TAG, "Offline User Added Error");
                }
                boolean checkUserExistse = dbHelper.checkUserExists();
                if(checkUserExistse == true) {
                    Log.v(TAG, "Offline User exists");
                    finish();
                } else {
                    Log.v(TAG, "Offline User does not exists");
                }
            }
        }

    }

}
