package pl.karolkniec.rewords.ui;

import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.util.DatabaseHelper;

public class SummaryActivity extends AppCompatActivity {

    private static final String TAG = "SummaryActivity";

    private Toolbar toolbar;
    private TextView correctedTextView, wrongTextView, finishedTextView, experienceTextView;

    private DatabaseHelper dbHelper;
    private FirebaseAuth mAuth;
    private FirebaseFirestore firebaseFirestore;

    private int correctedCount, wrongCount, finishedCount, experience;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        toolbar = (Toolbar) findViewById(R.id.summaryToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.summary_name));

        correctedTextView = (TextView) findViewById(R.id.summaryCorrected);
        wrongTextView = (TextView) findViewById(R.id.summaryWrong);
        finishedTextView = (TextView) findViewById(R.id.summaryFinished);
        experienceTextView = (TextView) findViewById(R.id.summaryExperience);

        Bundle bundle = getIntent().getExtras();
        correctedCount = bundle.getInt("goodAnswers");
        wrongCount = bundle.getInt("wrongAnswers");
        finishedCount = bundle.getInt("finishedWords");
        experience = finishedCount * 5;

        dbHelper = new DatabaseHelper(this);
        mAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        if(experience > 0) {
            addExperience();
        }
        startCorrectedTextAnimation();
    }

    private void addExperience() {
        long currentLevel = MainActivity.user.getLevel();
        long currentExp = MainActivity.user.getExperience();
        boolean isOffline = MainActivity.user.getIsOffline();
        int newExperience = experience;
        long expToNextLevel = currentLevel * 100;

        do {
            currentExp++;
            if(currentExp == expToNextLevel) {
                currentLevel++;
                expToNextLevel = currentLevel * 100;
                currentExp = 0;
            }
            newExperience--;
        } while(newExperience > 0);
        
        if(isOffline == true) {
            boolean result = dbHelper.updateUserLevel(MainActivity.user.getDisplayName(), currentLevel, currentExp);
            MainActivity.user.setLevel(currentLevel);
            MainActivity.user.setExperience(currentExp);
            if(result == true) {

            } else {
                Log.v(TAG, "Failed update to user info");
            }
        } else {
            updateUserLevelFirebase(currentLevel, currentExp);
        }
    }

    private void updateUserLevelFirebase(final long currentLevel, final long currentExp) {
        final FirebaseUser user = mAuth.getCurrentUser();
        if(user != null) {
            Map<String, Object> userMap = new HashMap<>();
            userMap.put("Level", currentLevel);
            userMap.put("Experience", currentExp);

            firebaseFirestore.collection("Users").document(user.getUid()).set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()) {
                        Log.v(TAG, "Successfully updated user information into database");
                        MainActivity.user.setLevel(currentLevel);
                        MainActivity.user.setExperience(currentExp);
                    } else {
                        String error = task.getException().getMessage();
                        Log.v(TAG, error);
                    }
                }
            });
        }
    }

    private void startCorrectedTextAnimation() {
        correctedTextView.setVisibility(View.VISIBLE);
        int duration = 1000;

        if(correctedCount <= 0) {
            duration = 1;
        }

        ValueAnimator animator = ValueAnimator.ofInt(0, correctedCount);
        animator.setDuration(duration);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                correctedTextView.setText(getString(R.string.repeat_words_corrected_text) + " " + animation.getAnimatedValue().toString());

                if((int)animation.getAnimatedValue() == correctedCount) {

                    try {
                        Thread.sleep(200);
                        startWrongTextAnimation();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        animator.start();
    }

    private void startWrongTextAnimation() {
        wrongTextView.setVisibility(View.VISIBLE);
        int duration = 1000;

        if(wrongCount <= 0) {
            duration = 1;
        }

        ValueAnimator animatorWrong = ValueAnimator.ofInt(0, wrongCount);
        animatorWrong.setDuration(duration);
        animatorWrong.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                wrongTextView.setText(getString(R.string.repeat_words_wrong_text) + " " + animation.getAnimatedValue().toString());

                if((int)animation.getAnimatedValue() == wrongCount) {

                    try {
                        Thread.sleep(200);
                        startFinishedTextAnimation();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        animatorWrong.start();
    }

    private void startFinishedTextAnimation() {
        finishedTextView.setVisibility(View.VISIBLE);
        int duration = 1500;

        if(finishedCount <= 0) {
            duration = 1;
        }

        ValueAnimator animatorFinished = ValueAnimator.ofInt(0, finishedCount);
        animatorFinished.setDuration(duration);
        animatorFinished.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                finishedTextView.setText(getString(R.string.repeat_words_finished_text) + " " + animation.getAnimatedValue().toString());

                if((int)animation.getAnimatedValue() == finishedCount) {

                    try {
                        Thread.sleep(200);
                        startExperienceTextAnimation();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        animatorFinished.start();
    }

    private void startExperienceTextAnimation() {
        int duration = 1000;

        if(experience <= 0) {
            duration = 1;
        }

        experienceTextView.setVisibility(View.VISIBLE);
        ValueAnimator animatorExperience = ValueAnimator.ofInt(0, experience);
        animatorExperience.setDuration(duration);
        animatorExperience.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                experienceTextView.setText(getString(R.string.summary_experience) + animation.getAnimatedValue().toString());
            }
        });
        animatorExperience.start();
    }

    public void closeSummary(View view) {
        finish();
    }
}
