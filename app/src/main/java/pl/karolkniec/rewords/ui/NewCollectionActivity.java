package pl.karolkniec.rewords.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.opencensus.tags.Tag;
import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.core.Language;
import pl.karolkniec.rewords.core.Word;
import pl.karolkniec.rewords.util.DatabaseHelper;
import pl.karolkniec.rewords.util.WordListAdapter;

public class NewCollectionActivity extends AppCompatActivity {

    private static final String TAG = "NewCollectionActivity";

    private Spinner spinnerFirstLang, spinnerSecondLang;
    private ListView wordsListView;
    private TextView baseWordTextView, translationTextView, collectionNameTextView;

    private WordListAdapter wordsAdapter;
    private ArrayList<Word> wordsList = new ArrayList<Word>();
    private ArrayList<String> languagesList = new ArrayList<>();
    ArrayList<Language> userLanguages;
    private DatabaseHelper dbHelper;

    private boolean isEditMode = false;
    private int collectionId = 0;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_collection);

        Toolbar toolbar = (Toolbar) findViewById(R.id.newCollectionToolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinnerFirstLang = (Spinner) findViewById(R.id.newCollectionSpinnerFirst);
        spinnerSecondLang = (Spinner) findViewById(R.id.newCollectionSpinnerSecond);
        wordsListView = (ListView) findViewById(R.id.newCollectionWordsListView);
        wordsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder alert = new AlertDialog.Builder(NewCollectionActivity.this);
                alert.setMessage(getString(R.string.new_collection_warning_to_delete_word))
                        .setCancelable(true)
                        .setPositiveButton(getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeWordFromList(position);
                            }
                        })
                        .setNegativeButton(getString(R.string.no_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alert.setTitle(getString(R.string.deleting_text));
                alert.show();
            }
        });

        dbHelper = new DatabaseHelper(this);

        userLanguages = dbHelper.getLanguages();

        if(userLanguages != null) {
            for(Language userLangueage : userLanguages) {
                languagesList.add(userLangueage.getName());
            }
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, languagesList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFirstLang.setAdapter(adapter);
        spinnerSecondLang.setAdapter(adapter);

        baseWordTextView = (TextView) findViewById(R.id.baseWordTextView);
        translationTextView = (TextView) findViewById(R.id.translationTextView);
        collectionNameTextView = (TextView) findViewById(R.id.newCollectionName);

        final Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            isEditMode = bundle.getBoolean("isEditMode");
            collectionNameTextView.setText(bundle.getString("collectionName"));
            collectionId = bundle.getInt("collectionId");
            wordsList = dbHelper.getWords(collectionId);

            spinnerFirstLang.post(new Runnable() {
                @Override
                public void run() {
                    spinnerFirstLang.setSelection(adapter.getPosition(bundle.getString("collectionBaseLang")));
                }
            });

            spinnerSecondLang.post(new Runnable() {
                @Override
                public void run() {
                    spinnerSecondLang.setSelection(adapter.getPosition(bundle.getString("collectionTranslationLang")));
                }
            });
        }

        if(isEditMode == true) {
            getSupportActionBar().setTitle(getString(R.string.new_collection_edit_mode_name));
        } else {
            getSupportActionBar().setTitle(getString(R.string.new_collectino_name));
        }

        wordsAdapter = new WordListAdapter(this, R.layout.two_column_adapter_view_layout, wordsList);
        wordsListView.setAdapter(wordsAdapter);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder closeAlert = new AlertDialog.Builder(NewCollectionActivity.this);
        closeAlert.setMessage(getString(R.string.new_collection_warning_to_end_adding))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.no_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        closeAlert.setTitle(getString(R.string.end_text));
        closeAlert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_collection_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if(item.getItemId() == R.id.newCollectionApply) {
            addCollectionToDatabase();
            return true;
        }
        if(item.getItemId() == R.id.newCollectionLanguages) {
            Intent languagesManagerIntent = new Intent(NewCollectionActivity.this, LanguagesManagerActivity.class);
            startActivity(languagesManagerIntent);
        }
        if(item.getItemId() == R.id.newCollectionHelp) {
            android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(this);
            dialog.setTitle(getString(R.string.help_title))
                    .setMessage(getString(R.string.help_new_collection))
                    .setNegativeButton(getString(R.string.ok_text), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }

        return false;
    }

    private void removeWordFromList(int id) {
        wordsList.remove(id);
        wordsListView.setAdapter(wordsAdapter);
    }

    public void addWordToTheList(View view) {
        String baseWord = baseWordTextView.getText().toString();
        String translation = translationTextView.getText().toString();

        if(!TextUtils.isEmpty(baseWord) && !TextUtils.isEmpty(translation)) {
            wordsList.add(new Word(baseWord, translation));
            wordsListView.setAdapter(wordsAdapter);

            baseWordTextView.setText("");
            translationTextView.setText("");

            baseWordTextView.requestFocus();
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    public void clearWordList(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(NewCollectionActivity.this);
        alert.setMessage(getString(R.string.cleaning_list_warning_text))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        wordsList.clear();
                        wordsListView.setAdapter(wordsAdapter);
                    }
                })
                .setNegativeButton(getString(R.string.no_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alert.setTitle(getString(R.string.cleaning_list_text));
        alert.show();
    }

    private void addCollectionToDatabase() {
        String collectionName = collectionNameTextView.getText().toString();

        if(!TextUtils.isEmpty(collectionName)) {
            if(!wordsList.isEmpty()) {
                int firstLang = userLanguages.get(spinnerFirstLang.getSelectedItemPosition()).getId();
                int secondLang =  userLanguages.get(spinnerSecondLang.getSelectedItemPosition()).getId();

                Log.v(TAG, "First Language Position: " + String.valueOf(firstLang));
                Log.v(TAG, "Second Language Position: " + String.valueOf(secondLang));

                boolean result = false;

                if(isEditMode == true) {
                    result = dbHelper.editCollection(collectionId, collectionName, wordsList, firstLang, secondLang);
                    if(result == false) {
                        Toast.makeText(this, getString(R.string.new_collection_problem_with_edit), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    result = dbHelper.addCollection(collectionName, wordsList, firstLang, secondLang);
                    if(result == false) {
                        Toast.makeText(this, getString(R.string.new_collection_problem_with_adding), Toast.LENGTH_SHORT).show();
                    }
                }

                if(result == true) {
                    finish();
                }
            } else {
                Toast.makeText(this, getString(R.string.new_collection_warning_empty_collection_list), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.new_collection_warning_empty_collection_name), Toast.LENGTH_SHORT).show();
        }
    }
}
