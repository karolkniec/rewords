package pl.karolkniec.rewords.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.core.Collection;
import pl.karolkniec.rewords.util.CollectionListAdapter;
import pl.karolkniec.rewords.util.DatabaseHelper;

public class LearnTestFragment extends Fragment {
    
    private static final String TAG = "LearnTestFragment";
    
    private static final int RC_NEW_COLLECTION = 10;

    private FloatingActionButton newCollectionBtn;
    private ListView collectionListView;

    private CollectionListAdapter collectionListAdapter;
    private DatabaseHelper dbHelper;
    private ArrayList<Collection> collectionsList;
    private String[] countries;

    public LearnTestFragment() {}

    @Override
    public void onResume() {
        super.onResume();

        refreshCollectionList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_learn_test, container, false);
        countries = getResources().getStringArray(R.array.countries_array);

        collectionListView = (ListView) view.findViewById(R.id.collectionsListView);
        collectionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent collectionDetailsIntent = new Intent(getActivity(), CollectionDetailsActivity.class);
                Bundle bundle = new Bundle();
                Collection collection = collectionsList.get(position);

                bundle.putInt("collectionId", collection.getId());
                bundle.putString("collectionName", collection.getName());
                bundle.putString("collectionBaseLanguage", collection.getBaseLanguage());
                bundle.putString("collectionTranslationLanguage", collection.getTranslationLanguage());
                bundle.putInt("collectionWordsCount", collection.getWordsCount());

                collectionDetailsIntent.putExtras(bundle);
                startActivity(collectionDetailsIntent);
            }
        });

        collectionListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final int collectionId = collectionsList.get(position).getId();
                Log.v(TAG, "Selected collection Id: " + String.valueOf(collectionId));

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCancelable(true)
                        .setPositiveButton(getString(R.string.edit_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.v(TAG, "Edit collection");

                                Intent newCollectionIntent = new Intent(getActivity(), NewCollectionActivity.class);
                                Bundle bundle = new Bundle();
                                Collection collection = collectionsList.get(position);

                                bundle.putInt("collectionId", collection.getId());
                                bundle.putString("collectionName", collection.getName());
                                bundle.putString("collectionBaseLang", collection.getBaseLanguage());
                                bundle.putString("collectionTranslationLang", collection.getTranslationLanguage());
                                bundle.putBoolean("isEditMode", true);

                                newCollectionIntent.putExtras(bundle);
                                startActivityForResult(newCollectionIntent, RC_NEW_COLLECTION);
                            }
                        })
                        .setNeutralButton(getString(R.string.delete_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.v(TAG, "Delete collection");
                                boolean removeResult = dbHelper.removeCollection(collectionId);
                                
                                if(removeResult == true) {
                                    Toast.makeText(getActivity(), getString(R.string.learn_test_positive_delete), Toast.LENGTH_SHORT).show();
                                    refreshCollectionList();
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.learn_test_negative_delete), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setMessage(getString(R.string.learn_test_alert_dialog_msg))
                        .setTitle(getString(R.string.learn_test_alert_dialog_title));
                alertDialog.show();

                return true;
            }
        });

        newCollectionBtn = (FloatingActionButton) view.findViewById(R.id.learnTestAddCollection);
        newCollectionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newCollectionIntent = new Intent(getActivity(), NewCollectionActivity.class);
                startActivityForResult(newCollectionIntent, RC_NEW_COLLECTION);
            }
        });

        dbHelper = new DatabaseHelper(getActivity());

        refreshCollectionList();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if(requestCode == RC_NEW_COLLECTION) {
            //Toast.makeText(getActivity(), "Dodano zbiór.", Toast.LENGTH_SHORT).show();
            refreshCollectionList();
        }
    }

    private void refreshCollectionList() {
        collectionsList = dbHelper.getCollections(countries);
        collectionListAdapter = new CollectionListAdapter(getActivity(), R.layout.collection_list_adapter_view_layout, collectionsList);
        collectionListView.setAdapter(collectionListAdapter);
    }
}
