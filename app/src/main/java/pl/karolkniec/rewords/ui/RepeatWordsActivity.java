package pl.karolkniec.rewords.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.core.Word;
import pl.karolkniec.rewords.util.DatabaseHelper;

public class RepeatWordsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView baseWordTextView,
            translationTextView,
            collectionNameTextView,
            goodAnswerTextView,
            wrongAnswerTextView,
            finishedWordsTextView;
    private EditText userAnswerEditText;
    private Button checkAnswerButton;

    private ArrayList<Word> wordsList;
    private DatabaseHelper dbHelper;
    private int currentWord;
    private boolean isCheckingAnswer = true;
    private boolean isRepeating = false;
    private int repeatMode = 0;

    private int wrongAnswerCount;
    private int finishedWordsCount;
    private int goodAnswerCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeat_words);

        toolbar = (Toolbar) findViewById(R.id.repeatWordsToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        baseWordTextView = (TextView) findViewById(R.id.repeatWordsBaseWord);
        translationTextView = (TextView) findViewById(R.id.repeatWordsTranslation);
        collectionNameTextView = (TextView) findViewById(R.id.repeatWordsCollectionName);
        userAnswerEditText = (EditText) findViewById(R.id.repeatWordsUserAnswer);
        userAnswerEditText.requestFocus();
        checkAnswerButton = (Button) findViewById(R.id.repeatWordsCheck);
        wrongAnswerTextView = (TextView) findViewById(R.id.repeatWrongAnswerTextView);
        finishedWordsTextView = (TextView) findViewById(R.id.repeatFinishedWordsTextView);
        goodAnswerTextView = (TextView) findViewById(R.id.repeatGoodAnswerTextView);

        Bundle bundle = getIntent().getExtras();
        int collectionId = bundle.getInt("collectionId");
        String collectionName = bundle.getString("collectionName");
        isRepeating = bundle.getBoolean("isRepeating");
        repeatMode = bundle.getInt("collectionRepeatMode");

        if(isRepeating == true) {
            getSupportActionBar().setTitle(getString(R.string.repeat_words_name));
        } else {
            getSupportActionBar().setTitle(getString(R.string.repeat_words_name_try_yourself));
        }

        dbHelper = new DatabaseHelper(this);
        wordsList = dbHelper.getWords(collectionId);

        currentWord = randomWord();

        if(repeatMode == 0) {
            baseWordTextView.setText(wordsList.get(currentWord).getBaseWord());
        } else {
            baseWordTextView.setText(wordsList.get(currentWord).getTranslation());
        }

        collectionNameTextView.setText(collectionName);

        wrongAnswerCount = 0;
        finishedWordsCount = 0;
        goodAnswerCount = 0;

        setRepeatCounters(wrongAnswerCount, goodAnswerCount, finishedWordsCount);
    }

    public void checkUserAnswer(View view) {
        if(checkItIsOver() == false) {
            if(isRepeating == false) {

                Intent summaryIntent = new Intent(RepeatWordsActivity.this, SummaryActivity.class);
                Bundle bundle = new Bundle();

                bundle.putInt("goodAnswers", goodAnswerCount);
                bundle.putInt("wrongAnswers", wrongAnswerCount);
                bundle.putInt("finishedWords", finishedWordsCount);

                summaryIntent.putExtras(bundle);
                startActivity(summaryIntent);
            }

            finish();
        }
        else if(isCheckingAnswer == true) {
            Word word = wordsList.get(currentWord);
            String userAnswer = userAnswerEditText.getText().toString().toLowerCase();
            String correctAnswer;
            if(repeatMode == 0) {
                correctAnswer = word.getTranslation();
            } else {
                correctAnswer = word.getBaseWord();
            }

            if(userAnswer.equals(correctAnswer.toLowerCase())) {
                translationTextView.setTextColor(Color.rgb(51, 204, 51));
                word.setHowManyRepeat(word.getHowManyRepeat() - 1);
                goodAnswerCount++;

                if(word.getHowManyRepeat() <= 0) {
                    finishedWordsCount++;
                }
            } else {
                translationTextView.setTextColor(Color.RED);

                if(isRepeating == false) {
                    word.setHowManyRepeat(word.getHowManyRepeat() - 1);
                } else {
                    word.setHowManyRepeat(word.getHowManyRepeat() + 2);
                }

                wrongAnswerCount++;
            }


            //Toast.makeText(this, String.valueOf(wordsList.get(currentWord).getHowManyRepeat()), Toast.LENGTH_SHORT).show();
            translationTextView.setText(correctAnswer);
            if(checkItIsOver() == true) {
                checkAnswerButton.setText(getString(R.string.next_text));
            } else {
                checkAnswerButton.setText(getString(R.string.finish_text));
            }

            userAnswerEditText.setInputType(InputType.TYPE_NULL);

            isCheckingAnswer = false;
            setRepeatCounters(wrongAnswerCount, goodAnswerCount, finishedWordsCount);

        } else {
            currentWord = randomWord();
            checkAnswerButton.setText(getString(R.string.check_text));
            isCheckingAnswer = true;

            if(repeatMode == 0) {
                baseWordTextView.setText(wordsList.get(currentWord).getBaseWord());
            } else {
                baseWordTextView.setText(wordsList.get(currentWord).getTranslation());
            }

            userAnswerEditText.setText("");
            userAnswerEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);

            translationTextView.setText("???");
            translationTextView.setTextColor(Color.BLACK);
        }
    }

    private boolean checkItIsOver() {
        for(Word word : wordsList) {
            if(word.getHowManyRepeat() > 0) {
                return true;
            }
        }

        return false;
    }

    private int randomWord() {
        Random random = new Random();
        int randNum = 0;

        do {
            randNum = random.nextInt(wordsList.size());
        } while(wordsList.get(randNum).getHowManyRepeat() <= 0);

        return randNum;
    }

    private void setRepeatCounters(int wrong, int good, int finished) {
        goodAnswerTextView.setText(getString(R.string.repeat_words_corrected_text) + good);
        wrongAnswerTextView.setText(getString(R.string.repeat_words_wrong_text) + wrong);
        finishedWordsTextView.setText(getString(R.string.repeat_words_finished_text) + finished + " / " + wordsList.size());
    }

    @Override
    public void onBackPressed() {
        alertDialogExit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return false;
    }

    public void stopRepeating(View view) {
        alertDialogExit();
    }

    private void alertDialogExit() {
        AlertDialog.Builder closeAlert = new AlertDialog.Builder(RepeatWordsActivity.this);
        closeAlert.setMessage(getString(R.string.repeat_words_warning_to_finish_repeating))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.no_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        closeAlert.setTitle(getString(R.string.end_text));
        closeAlert.show();
    }
}
