package pl.karolkniec.rewords.ui;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;


import de.hdodenhof.circleimageview.CircleImageView;
import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.util.DatabaseHelper;

public class AccountFragment extends Fragment {

    private static final String TAG = "AccountFragment";

    private FirebaseAuth mAuth;

    private DatabaseHelper dbHelper;

    private Button signOutBtn;
    private TextView usernameTextView, accountLevel, accountExperience;
    private CircleImageView userPhoto;
    private ProgressBar accountExpProgressBar;

    public AccountFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_account, container, false);

        mAuth = FirebaseAuth.getInstance();
        dbHelper = new DatabaseHelper(getActivity());

        usernameTextView = (TextView) view.findViewById(R.id.accountUsername);
        usernameTextView.setText(MainActivity.user.getDisplayName());

        accountLevel = (TextView) view.findViewById(R.id.accountLevel);
        accountLevel.setText("Level " + MainActivity.user.getLevel());

        accountExperience = (TextView) view.findViewById(R.id.accountExperience);
        accountExperience.setText(MainActivity.user.getExperience() + " / " + MainActivity.user.getLevel() * 100);

        accountExpProgressBar = (ProgressBar) view.findViewById(R.id.accountExpProgressBar);

        userPhoto = (CircleImageView) view.findViewById(R.id.accountUserPhoto);

        Handler progressBarHandler = new Handler();

        progressBarHandler.post(new Runnable() {
            @Override
            public void run() {
                accountExpProgressBar.setMax((int)MainActivity.user.getLevel() * 100);
                accountExpProgressBar.setProgress((int)MainActivity.user.getExperience());
            }
        });

        Log.v(TAG, "Max progress bar: " + String.valueOf(accountExpProgressBar.getMax()));
        Log.v(TAG, "Current progress: " + String.valueOf(accountExpProgressBar.getProgress()));

        RequestOptions placeholderRequest = new RequestOptions();
        placeholderRequest.placeholder(R.drawable.default_user);

        Glide.with(this).setDefaultRequestOptions(placeholderRequest).load(MainActivity.user.getPhotoUrl()).into(userPhoto);
        Log.i("PhotoUser", MainActivity.user.getPhotoUrl().toString());

        signOutBtn = (Button) view.findViewById(R.id.signOut);
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.user.getIsOffline() == false) {
                    mAuth.signOut();
                    Intent startupIntent = new Intent(getActivity(), StartupActivity.class);
                    startActivity(startupIntent);
                } else {
                    boolean isSignOut = dbHelper.signOut(MainActivity.user.getDisplayName());
                    if(isSignOut == true) {
                        Intent startupIntent = new Intent(getActivity(), StartupActivity.class);
                        startActivity(startupIntent);
                    }
                }
                MainActivity.user = null;
            }
        });

        return view;
    }

}
