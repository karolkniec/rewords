package pl.karolkniec.rewords.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.core.Language;
import pl.karolkniec.rewords.util.DatabaseHelper;

public class LanguagesManagerActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText nameLangEditText;
    DatabaseHelper dbHelper;
    ListView languagesListView;
    ArrayAdapter<String> arrayAdapter;
    ArrayList<String> languagesList = new ArrayList<>();
    ArrayList<Language> userLanguages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languages_manager);

        toolbar = (Toolbar) findViewById(R.id.languageManagerToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.languages_manager_title));

        dbHelper = new DatabaseHelper(this);

        languagesListView = (ListView) findViewById(R.id.languagesManagerList);

        refreshLanguagesList();

        languagesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final int languageId = userLanguages.get(position).getId();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LanguagesManagerActivity.this);
                alertDialog.setTitle(getString(R.string.languages_manager_manage_dialog_title))
                        .setMessage(getString(R.string.languages_manager_manage_dialog_msg))
                        .setPositiveButton(getString(R.string.edit_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final AlertDialog.Builder mDialog = new AlertDialog.Builder(LanguagesManagerActivity.this);
                                View mView = getLayoutInflater().inflate(R.layout.add_language_dialog, null);
                                nameLangEditText = (EditText) mView.findViewById(R.id.languageName);
                                TextView languageDialogTitle = (TextView) mView.findViewById(R.id.languageDialogTitle);

                                nameLangEditText.setText(userLanguages.get(position).getName());
                                languageDialogTitle.setText(getString(R.string.lanugages_manager_dialog_title_edit));

                                mDialog.setPositiveButton(getString(R.string.edit_text), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String langName = nameLangEditText.getText().toString();

                                        if(!langName.isEmpty()) {
                                            boolean result = dbHelper.editLanguage(languageId, langName);
                                            if(result) {
                                                refreshLanguagesList();
                                            }
                                        }
                                    }
                                });

                                mDialog.setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                                mDialog.setView(mView);
                                AlertDialog dialogEdit = mDialog.create();
                                dialogEdit.show();

                            }
                        })
                        .setNeutralButton(getString(R.string.delete_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AlertDialog.Builder deleteDialog = new AlertDialog.Builder(LanguagesManagerActivity.this);
                                deleteDialog.setTitle(getString(R.string.deleting_text))
                                        .setMessage(getString(R.string.are_you_sure_text))
                                        .setPositiveButton(getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                boolean result = dbHelper.removeLanguage(languageId);

                                                if(result) {
                                                    refreshLanguagesList();
                                                }
                                            }
                                        })
                                        .setNegativeButton(getString(R.string.no_text), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                deleteDialog.show();
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.languages_manager_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.languagesManagerAdd:
                showAddLanguageDialog();
                return true;
            case R.id.languagesManagerHelp:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle(getString(R.string.help_title))
                        .setMessage(getString(R.string.help_languages_manager))
                        .setNegativeButton(getString(R.string.ok_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                return true;
        }

        return false;
    }

    private void refreshLanguagesList() {
        userLanguages = dbHelper.getLanguages();
        languagesList.clear();

        if(userLanguages != null) {
            for(Language userLangueage : userLanguages) {
                languagesList.add(userLangueage.getName());
            }
        }

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, languagesList);
        languagesListView.setAdapter(arrayAdapter);
    }

    private void showAddLanguageDialog() {
        final AlertDialog.Builder mDialog = new AlertDialog.Builder(LanguagesManagerActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.add_language_dialog, null);
        nameLangEditText = (EditText) mView.findViewById(R.id.languageName);

        mDialog.setPositiveButton(getString(R.string.add_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String langName = nameLangEditText.getText().toString();

                if(!langName.isEmpty()) {
                    boolean result = dbHelper.addLanguage(langName);
                    if(result) {
                        refreshLanguagesList();
                    }
                }
            }
        });

        mDialog.setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mDialog.setView(mView);
        AlertDialog dialog = mDialog.create();
        dialog.show();
    }
}
