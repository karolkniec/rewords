package pl.karolkniec.rewords.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.core.User;
import pl.karolkniec.rewords.util.DatabaseHelper;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private FrameLayout mMainFrame;
    private Toolbar mToolbar;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseFirestore firebaseFirestore;

    public static User user;
    private DatabaseHelper dbHelper;

    private ReadyContentFragment readyContentFragment;
    private LearnTestFragment learnTestFragment;
    private AccountFragment accountFragment;

    @Override
    protected void onStart() {
        super.onStart();

        int offlineUserIsSignIn = 0;
        Cursor offlineUser = null;


        if(dbHelper.checkUserExists() == true) {
            offlineUser = dbHelper.getUser();
            if(offlineUser.moveToFirst()) {
                offlineUserIsSignIn = offlineUser.getInt(offlineUser.getColumnIndex("IsSignIn"));
            }
        }

        if(offlineUserIsSignIn == 1) {
            if(offlineUser.moveToFirst()) {

                user = User.createUser(
                        "",
                        offlineUser.getString(offlineUser.getColumnIndex("Username")),
                        Uri.EMPTY,
                        offlineUser.getInt(offlineUser.getColumnIndex("Level")),
                        offlineUser.getInt(offlineUser.getColumnIndex("Experience")),
                        true
                );
            }
        }

        if(user == null) {
            final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

            if(currentUser == null) {
                sendToStartupActivity();
            } else {
                firebaseFirestore.collection("Users").document(currentUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()) {
                            if(task.getResult().exists()) {

                                user = User.createUser(
                                        currentUser.getEmail(),
                                        currentUser.getDisplayName(),
                                        currentUser.getPhotoUrl(),
                                        task.getResult().getLong("Level"),
                                        task.getResult().getLong("Experience"),
                                        false
                                );

                            }
                        }
                    }
                });
            }
        }

        setFragment(learnTestFragment);
        mMainNav.setSelectedItemId(R.id.navLearnTest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DatabaseHelper(this);

        mToolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));

        firebaseFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        mMainFrame = (FrameLayout) findViewById(R.id.mainFrame);
        mMainNav = (BottomNavigationView) findViewById(R.id.mainNav);

        readyContentFragment = new ReadyContentFragment();
        learnTestFragment = new LearnTestFragment();
        accountFragment = new AccountFragment();

        setFragment(learnTestFragment);

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navLearnTest:
                        setFragment(learnTestFragment);
                        return true;
                    case R.id.navAccount:
                        if(user != null) {
                            setFragment(accountFragment);
                        }
                        return true;
                    case R.id.navReadyContent:
                        setFragment(readyContentFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.mainMenuRemoveProfile:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle(getString(R.string.remove_profile_title))
                        .setMessage(getString(R.string.remove_profile_warning))
                        .setCancelable(true)
                        .setPositiveButton(getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeProfile();
                            }
                        })
                        .setNegativeButton(getString(R.string.no_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return true;
            case R.id.mainManuLanguages:
                Intent languagesManagerIntent = new Intent(MainActivity.this, LanguagesManagerActivity.class);
                startActivity(languagesManagerIntent);
                return true;
            case R.id.mainMenuHelp:
                android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(this);
                dialog.setTitle(getString(R.string.help_title))
                        .setMessage(getString(R.string.help_main_menu))
                        .setNegativeButton(getString(R.string.ok_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                return true;
        }

        return false;
    }

    private void removeProfile() {
        if(MainActivity.user.getIsOffline() == true) {
            boolean result = dbHelper.removeUser();

            if(result == true) {
                Toast.makeText(this, getString(R.string.removed_profile_info), Toast.LENGTH_SHORT).show();
                MainActivity.user = null;
                sendToStartupActivity();
            } else {
                Toast.makeText(this, getString(R.string.removed_profile_error_info), Toast.LENGTH_SHORT).show();
            }
        } else {
            firebaseFirestore.collection("Users").document(mAuth.getCurrentUser().getUid()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()) {
                        mAuth.signOut();
                        MainActivity.user = null;
                        Toast.makeText(MainActivity.this, getString(R.string.removed_profile_info), Toast.LENGTH_SHORT).show();
                        sendToStartupActivity();
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.removed_profile_error_info), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void sendToStartupActivity() {
        Intent startupIntent = new Intent(MainActivity.this, StartupActivity.class);
        startActivity(startupIntent);
    }

    public void startNewCollectionActivity() {
        Intent newCollectionActivity = new Intent(MainActivity.this, NewCollectionActivity.class);
        startActivity(newCollectionActivity);
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.commit();
    }
}
