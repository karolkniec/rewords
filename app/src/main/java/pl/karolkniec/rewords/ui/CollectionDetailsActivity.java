package pl.karolkniec.rewords.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.core.Collection;
import pl.karolkniec.rewords.core.Word;
import pl.karolkniec.rewords.util.DatabaseHelper;
import pl.karolkniec.rewords.util.WordListAdapter;

public class CollectionDetailsActivity extends AppCompatActivity {

    private static final int RC_NEW_COLLECTION = 10;

    private Toolbar toolbar;
    private ListView wordsListView;
    private TextView nameTextView;
    private Spinner languagesSpinner;

    private Collection collection;
    private ArrayList<Word> wordsList;
    private DatabaseHelper dbHelper;
    private WordListAdapter wordsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_details);

        wordsListView = (ListView) findViewById(R.id.collectionDetialsWordsListView);

        nameTextView = (TextView) findViewById(R.id.collectionDetailsName);
        languagesSpinner = (Spinner) findViewById(R.id.collectionDetailsLanguages);

        toolbar = (Toolbar) findViewById(R.id.collectionDetailsToolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.collection_details_name));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DatabaseHelper(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {

            collection = Collection.create(
                    bundle.getInt("collectionId"),
                    bundle.getString("collectionName"),
                    bundle.getString("collectionBaseLanguage"),
                    bundle.getString("collectionTranslationLanguage"),
                    bundle.getInt("collectionWordsCount")
            );

            wordsList = dbHelper.getWords(collection.getId());
        }

        ArrayList<String> repeatModeList = new ArrayList<>();
        repeatModeList.add(0, collection.getBaseLanguage() + " -> " + collection.getTranslationLanguage());
        repeatModeList.add(1, collection.getTranslationLanguage() + " -> " + collection.getBaseLanguage());

        nameTextView.setText(collection.getName());
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_black, repeatModeList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languagesSpinner.setAdapter(spinnerAdapter);

        wordsAdapter = new WordListAdapter(this, R.layout.two_column_adapter_view_layout, wordsList, Color.BLACK);
        wordsListView.setAdapter(wordsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.collection_details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.collectionDetailsEdit:
                editCollection();
                return true;
            case R.id.collectionDetailsRemove:
                deleteCollection();
                return true;
        }

        return false;
    }

    public void startRepeating(View view) {
        Intent repeatIntent = new Intent(CollectionDetailsActivity.this, RepeatWordsActivity.class);
        Bundle bundle = new Bundle();

        bundle.putInt("collectionId", collection.getId());
        bundle.putString("collectionName", collection.getName());
        bundle.putBoolean("isRepeating", true);
        bundle.putInt("collectionRepeatMode", languagesSpinner.getSelectedItemPosition());

        repeatIntent.putExtras(bundle);
        startActivity(repeatIntent);
        finish();
    }

    public void startTryYourself(View view) {
        Intent repeatIntent = new Intent(CollectionDetailsActivity.this, RepeatWordsActivity.class);
        Bundle bundle = new Bundle();

        bundle.putInt("collectionId", collection.getId());
        bundle.putString("collectionName", collection.getName());
        bundle.putBoolean("isRepeating", false);
        bundle.putInt("collectionRepeatMode", languagesSpinner.getSelectedItemPosition());

        repeatIntent.putExtras(bundle);
        startActivity(repeatIntent);
        finish();
    }

    private void editCollection() {
        Intent newCollectionIntent = new Intent(CollectionDetailsActivity.this, NewCollectionActivity.class);
        Bundle bundle = new Bundle();

        bundle.putInt("collectionId", collection.getId());
        bundle.putString("collectionName", collection.getName());
        bundle.putString("collectionBaseLang", collection.getBaseLanguage());
        bundle.putString("collectionTranslationLang", collection.getTranslationLanguage());
        bundle.putBoolean("isEditMode", true);

        newCollectionIntent.putExtras(bundle);
        startActivity(newCollectionIntent);
        finish();
    }

    private void deleteCollection() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.deleting_text))
                .setMessage(getString(R.string.waring_to_delete))
                .setPositiveButton(getString(R.string.yes_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean removeResult = dbHelper.removeCollection(collection.getId());

                        if(removeResult == true) {
                            Toast.makeText(CollectionDetailsActivity.this, getString(R.string.learn_test_positive_delete), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(CollectionDetailsActivity.this, getString(R.string.learn_test_negative_delete), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.no_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
