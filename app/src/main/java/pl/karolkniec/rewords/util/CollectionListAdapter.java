package pl.karolkniec.rewords.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.core.Collection;

public class CollectionListAdapter extends ArrayAdapter<Collection> {

    private static final String TAG = "CollectionListAdapter";

    private Context mContext;
    private int mResource;
    private ArrayList<Collection> mObjects;

    public CollectionListAdapter( Context context, int resource, ArrayList<Collection> objects) {
        super(context, resource, objects);

        mContext = context;
        mResource = resource;
        mObjects = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        String name = getItem(position).getName();
        String baseLanguage = getItem(position).getBaseLanguage();
        String translationLanguage = getItem(position).getTranslationLanguage();
        int wordsCount = getItem(position).getWordsCount();

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView nameTextView = (TextView) convertView.findViewById(R.id.collectionName);
        TextView languagesTextView = (TextView) convertView.findViewById(R.id.collectionLanguages);
        TextView wordsCountTextView = (TextView) convertView.findViewById(R.id.collectionWordsCount);

        nameTextView.setText(name);
        languagesTextView.setText(baseLanguage + " -> " + translationLanguage);
        wordsCountTextView.setText(mContext.getString(R.string.learn_text_words_count) + " " + String.valueOf(wordsCount));

        return convertView;
    }
}
