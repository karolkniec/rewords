package pl.karolkniec.rewords.util;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pl.karolkniec.rewords.core.Word;
import pl.karolkniec.rewords.R;
import pl.karolkniec.rewords.ui.MainActivity;

public class WordListAdapter extends ArrayAdapter<Word> {

    private static final String TAG = "WordListAdapter";

    private Context mContext;
    private int mResource;
    private ArrayList<Word> mObjects;
    int textColor;

    public WordListAdapter(Context context, int resource, ArrayList<Word> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        mObjects = objects;
        textColor = Color.WHITE;
    }

    public WordListAdapter(Context context, int resource, ArrayList<Word> objects, int textColor) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        mObjects = objects;
        this.textColor = textColor;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        String baseWord = getItem(position).getBaseWord();
        String translation = getItem(position).getTranslation();

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView firstWordText = (TextView) convertView.findViewById(R.id.firstWordText);
        TextView secondWordText = (TextView) convertView.findViewById(R.id.secondWordText);

        firstWordText.setTextColor(textColor);
        secondWordText.setTextColor(textColor);

        firstWordText.setText(baseWord);
        secondWordText.setText(translation);

        return convertView;
    }
}
