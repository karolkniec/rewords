package pl.karolkniec.rewords.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import pl.karolkniec.rewords.core.Collection;
import pl.karolkniec.rewords.core.Language;
import pl.karolkniec.rewords.core.Word;
import pl.karolkniec.rewords.ui.MainActivity;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String DATABASE_NAME = "Rewords.db3";
    private static final int DATABASE_VERSION = 2;

    private static final String USERS_TABLE = "Users";
    private static final String COLLECTIONS_TABLE = "Collections";
    private static final String WORDS_TABLE = "Words";
    private static final String LANGUAGES_TABLE = "Languages";

    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        ArrayList<String> queries = new ArrayList<String>();
        queries.add("CREATE TABLE " + USERS_TABLE + "(Id INTEGER PRIMARY KEY AUTOINCREMENT, Username VARCHAR(100), Level INTEGER, Experience INTEGER, IsSignIn INTEGER)");
        queries.add("CREATE TABLE " + COLLECTIONS_TABLE + "(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR(100), BaseLanguage INTEGER, " +
                "TranslationLanguage INTEGER)");
        queries.add("CREATE TABLE " + WORDS_TABLE + "(Id INTEGER PRIMARY KEY AUTOINCREMENT, BaseWord VARCHAR(100), Translation VARCHAR(100), " +
                "CollectionId INTEGER)");
        queries.add("CREATE TABLE " + LANGUAGES_TABLE + "(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR(100))");
        queries.add("INSERT INTO " + LANGUAGES_TABLE + "(Id, Name) VALUES(null, 'Polish'), (null, 'English'), (null, 'German')");

        for(String q : queries) {
            db.execSQL(q);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ArrayList<String> queries = new ArrayList<String>();
        queries.add("CREATE TABLE " + LANGUAGES_TABLE + "(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR(100))");
        queries.add("INSERT INTO " + LANGUAGES_TABLE + "(Id, Name) VALUES(null, 'Polish'), (null, 'English'), (null, 'German')");

        for(String q : queries) {
            db.execSQL(q);
        }
    }

    public boolean addUser(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Id", 0);
        contentValues.put("Username", username);
        contentValues.put("Level", 1);
        contentValues.put("Experience", 0);
        contentValues.put("IsSignIn", 1);

        long result = db.insert(USERS_TABLE, null, contentValues);

        if(result == -1){
            return false;
        }

        return true;
    }

    public boolean removeUser() {
        SQLiteDatabase db = this.getWritableDatabase();

        long result = db.delete(USERS_TABLE, "", null);

        if(result == -1) {
            return false;
        }

        return true;
    }

    public Cursor getUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + USERS_TABLE;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public boolean updateUserLevel(String username, long currentLevel, long currentExp) {
        SQLiteDatabase db= this.getWritableDatabase();
        String query = "UPDATE " + USERS_TABLE + " SET Level = " + currentLevel + ", Experience = " + currentExp + " WHERE Username = " + "'" + username + "'";
        db.execSQL(query);

        query = "SELECT * FROM " + USERS_TABLE + " WHERE Username = " +  "'" + username + "'";
        Cursor user = db.rawQuery(query, null);

        if(user.moveToFirst()) {
            long level = user.getInt(user.getColumnIndex("Level"));
            long experience = user.getLong(user.getColumnIndex("Experience"));

            if(level == currentLevel && experience == currentExp) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public boolean addCollection(String name, ArrayList<Word> words, int firstLanguage, int secondLanguage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues collectionValues = new ContentValues();
        collectionValues.put("Name", name);
        collectionValues.put("BaseLanguage", firstLanguage);
        collectionValues.put("TranslationLanguage", secondLanguage);

        long collectionResult = db.insert(COLLECTIONS_TABLE, null, collectionValues);

        if(collectionResult != -1) {

            String query = "SELECT * FROM " + COLLECTIONS_TABLE + " WHERE Name = " + "'" + name + "'";
            Cursor collectionData = db.rawQuery(query, null);
            int collectionId = 0;

            if (collectionData.moveToFirst()) {
                collectionId = collectionData.getInt(collectionData.getColumnIndex("Id"));
            }

            for(Word word : words) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("BaseWord", word.getBaseWord());
                contentValues.put("Translation", word.getTranslation());
                contentValues.put("CollectionId", collectionId);

                long wordResult = db.insert(WORDS_TABLE, null, contentValues);

                if(wordResult == -1) {
                    return false;
                }
            }

        } else {
            return false;
        }

        return true;
    }

    public boolean editCollection(int collectionId, String name, ArrayList<Word> words, int firstLanguage, int secondLanguage) {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean removeResult = removeCollection(collectionId);

        if(removeResult == false) {
            return false;
        }

        boolean addResult = addCollection(name, words, firstLanguage, secondLanguage);

        if(addResult == false) {
            return false;
        }

        return true;
    }

    public ArrayList<Collection> getCollections(String[] countries) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Collection> collectionsList = new ArrayList<Collection>();

        String query = "SELECT * FROM " + COLLECTIONS_TABLE;
        Cursor collections = db.rawQuery(query, null);

        while(collections.moveToNext()) {
            int collectionId = collections.getInt(collections.getColumnIndex("Id"));
            Log.v(TAG, "Collection Id: " + String.valueOf(collectionId));

            String wordsQuery = "SELECT CollectionId FROM " + WORDS_TABLE + " WHERE CollectionId = " + collectionId;
            Cursor wordsData = db.rawQuery(wordsQuery, null);
            while(wordsData.moveToNext()) {
                Log.v(TAG, "CollectionId from Word: " + String.valueOf(wordsData.getInt(0)));
            }

            int id = collections.getInt(collections.getColumnIndex("Id"));
            String name = collections.getString(collections.getColumnIndex("Name"));
            int baseLanguageId = collections.getInt(collections.getColumnIndex("BaseLanguage"));
            int translationLanguageId = collections.getInt(collections.getColumnIndex("TranslationLanguage"));
            int wordsCount = wordsData.getCount();

            ArrayList<Language> languages = getLanguages();

            String baseLanguage = "";
            for (Language lang : languages) {
                if(lang.getId() == baseLanguageId) {
                    baseLanguage = lang.getName();
                }
            }

            String translationLanguage = "";
            for (Language lang : languages) {
                if (lang.getId() == translationLanguageId) {
                    translationLanguage = lang.getName();
                }
            }

            collectionsList.add(Collection.create(
                    id,
                    name,
                    baseLanguage,
                    translationLanguage,
                    wordsCount
            ));
        }

        return collectionsList;
    }

    public boolean removeCollection(int collectionId) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(WORDS_TABLE, "CollectionId = " + collectionId, null);

        if(result == -1) {
            return false;
        }

        result = db.delete(COLLECTIONS_TABLE, "Id = " + collectionId, null);

        if(result == -1) {
            return false;
        }

        return true;
    }

    public ArrayList<Word> getWords(int collectionId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Word> wordsList = new ArrayList<>();
        String wordsQuery = "SELECT * FROM " + WORDS_TABLE + " WHERE CollectionId = " + collectionId;
        Cursor wordsData = db.rawQuery(wordsQuery, null);

        while(wordsData.moveToNext()) {
            String baseWord = wordsData.getString(wordsData.getColumnIndex("BaseWord"));
            String translation = wordsData.getString(wordsData.getColumnIndex("Translation"));

            wordsList.add(new Word(baseWord, translation));
        }

        return wordsList;
    }

    public boolean signOut(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + USERS_TABLE + " SET isSignIn = 0 WHERE Username = "  +  "'" + username + "'";
        db.execSQL(query);

        query = "SELECT * FROM " + USERS_TABLE + " WHERE Username = " +  "'" + username + "'";
        Cursor user = db.rawQuery(query, null);
        int isSignIn = 1;
        if(user.moveToFirst()) {
            isSignIn = user.getInt(user.getColumnIndex("IsSignIn"));
        }

        if(isSignIn == 1) {
            return false;
        }

        return true;
    }

    public boolean signInOffline() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor user = getUser();
        String username = "";

        if(user.moveToFirst()) {
            username = user.getString(user.getColumnIndex("Username"));
        }


        String query = "UPDATE " + USERS_TABLE + " SET isSignIn = 1 WHERE Username = "  +  "'" + username + "'";
        db.execSQL(query);

        query = "SELECT * FROM " + USERS_TABLE + " WHERE Username = " +  "'" + username + "'";
        user = db.rawQuery(query, null);
        int isSignIn = 1;
        if(user.moveToFirst()) {
            isSignIn = user.getInt(user.getColumnIndex("IsSignIn"));
        }

        if(isSignIn == 0) {
            return false;
        }

        return true;
    }

    public boolean checkUserExists() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + USERS_TABLE;
        Cursor data = db.rawQuery(query, null);
        if(data.getCount() == 0) {
            return false;
        }

        return true;
    }

    public boolean addLanguage(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Name", name);

        long result = db.insert(LANGUAGES_TABLE, null, values);

        if(result == -1) {
            return false;
        }

        return true;
    }

    public boolean editLanguage(int languageId, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Name", name);

        return db.update(LANGUAGES_TABLE, values, "Id = " + languageId, null) > 0;
    }

    public boolean removeLanguage(int languageId) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(LANGUAGES_TABLE, "Id = " + languageId, null);

        if(result == -1) {
            return false;
        }

        return true;
    }

    public ArrayList<Language> getLanguages() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + LANGUAGES_TABLE;
        ArrayList<Language> languagesList = new ArrayList<Language>();

        Cursor languages = db.rawQuery(query, null);
        while(languages.moveToNext()) {
            languagesList.add(Language.create(languages.getInt(0), languages.getString(1)));
        }

        return languagesList;
    }
}
