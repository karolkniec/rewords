package pl.karolkniec.rewords.core;

public class Language {
    private int id;
    private String name;

    protected Language(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public static Language create(int id, String name) {
        return new Language(id, name);
    }
}
