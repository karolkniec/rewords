package pl.karolkniec.rewords.core;

public class Collection {
    private int id;
    private String name;
    private String baseLanguage;
    private String translationLanguage;
    private int wordsCount;

    private Collection(int id, String name, String baseLanguage, String translationLanguage, int wordsCount) {
        this.id = id;
        this.name = name;
        this.baseLanguage = baseLanguage;
        this.translationLanguage = translationLanguage;
        this.wordsCount = wordsCount;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getBaseLanguage() {
        return this.baseLanguage;
    }

    public String getTranslationLanguage() {
        return this.translationLanguage;
    }

    public int getWordsCount() {
        return this.wordsCount;
    }

    public static Collection create(int id, String name, String baseLanguage, String translationLanguage, int wordsCount) {
        return new Collection(id, name, baseLanguage, translationLanguage, wordsCount);
    }
}
