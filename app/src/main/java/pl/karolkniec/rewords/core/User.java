package pl.karolkniec.rewords.core;

import android.net.Uri;

public class User {
    private String email;
    private String displayName;
    private Uri photoUrl;
    private long level;
    private long experience;
    private boolean isOffline;

    /**
     *
     * @param email
     * @param displayName
     * @param photoUrl
     * @param level
     * @param experience
     * @param isOffline
     */
    private User(String email, String displayName, Uri photoUrl, long level, long experience, boolean isOffline) {
        this.email = email;
        this.displayName = displayName;
        this.photoUrl = photoUrl;
        this.level = level;
        this.experience = experience;
        this.isOffline = isOffline;
    }

    public String getEmail() {
        return this.email;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public Uri getPhotoUrl() {
        return this.photoUrl;
    }

    public long getLevel() {
        return this.level;
    }

    public void setLevel(long level) {
        this.level = level;
    }

    public long getExperience() {
        return this.experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public boolean getIsOffline() {
        return this.isOffline;
    }

    /**
     *
     * @param email
     * @param displayName
     * @param photoUrl
     * @param level
     * @param experience
     * @param isOffline
     * @return
     */
    public static User createUser(String email, String displayName, Uri photoUrl, long level, long experience, boolean isOffline) {
        return new User(email, displayName, photoUrl, level, experience, isOffline);
    }
}
