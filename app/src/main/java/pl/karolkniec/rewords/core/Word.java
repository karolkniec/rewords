package pl.karolkniec.rewords.core;

public class Word {
    private String baseWord;
    private String translation;
    private int howManyRepeat;

    public Word(String baseWord, String translation) {
        this.baseWord = baseWord;
        this.translation = translation;
        this.howManyRepeat = 1;
    }

    public String getBaseWord() {
        return baseWord;
    }

    public String getTranslation() {
        return translation;
    }

    public int getHowManyRepeat() {
        return howManyRepeat;
    }

    public void setHowManyRepeat(int howManyRepeat) {
        this.howManyRepeat = howManyRepeat;
    }
}
